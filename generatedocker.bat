rem build polymer
rem polymer build

rem kill old docker
docker kill gui

rem remove old docker
docker rm gui

rem create a new build
docker build -t mlopez/gui .

rem start new container
rem --net techui
docker run -p 8081:2900 --name gui -d mlopez/gui
