#Imagen base
FROM node:9.2.0

# Directorio de la app
WORKDIR /app

#copiar archivos del proyecto a la carpeta que va a usar docker
ADD build/es6-bundled /app/build/default
ADD package.json /app
ADD server.js /app

#dependencias
RUN npm install

#puerto
EXPOSE 2900

# comando de arranque
CMD ["npm", "start"]
