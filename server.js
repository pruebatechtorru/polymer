var express = require('express'),
  app = express(),
  port = process.env.PORT || 2900;

  var path = require('path');

app.use(express.static(__dirname + '/build/default'));

app.listen(port);


console.log('Polymer y node: ' + port);

app.get('/', function(request, response){
  response.sendFile("index.html",{root: '.'});
});
